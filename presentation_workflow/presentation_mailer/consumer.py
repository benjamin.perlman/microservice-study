import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body, request):

    data = json.loads(body)
    email = data["presenter_email"]
    name = data["presenter_name"]
    title = data["title"]
    send_mail(
        "Your presentation has been approved",
        f"{name}, we're writing to tell you that your presentation {title} has been accepted",
        ' "admin@conference.go"',
        [email],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body, request):
    # print("  Received %r" % body)
    print("thanks")
    data = json.loads(body)
    email = data["presenter_email"]
    name = data["presenter_name"]
    title = data["title"]
    send_mail(
        "Your presentation has been rejected",
        f"{name}, we're writing to tell you that your presentation {title} has not been accepted",
        ' "admin@conference.go"',
        [email],
        fail_silently=False,
    )


while True:
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")

    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=process_approval,
        auto_ack=True,
    )
    channel.queue_declare(queue="presentation_rejections")

    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=process_rejection,
        auto_ack=True,
    )
    print("consuming")
    channel.start_consuming()
